import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const keys = new Map();
    keys.set(e.code, true);
    const PlayerOne {
      ...firstFighter,
      statusBlocking: false
    };
    const PlayerTwo {
      ...secondFighter,
      statusBlocking: false
    };

    document.addEventListener('keydown', (e) => {
     
        if (e.code === controls.PlayerOneBlock) {
          FighterBlocking(PlayerOne);
        }
        if (e.code === controls.PlayerTwoBlock) {
          FighterBlocking(PlayerTwo);
        }
        if (e.code === controls.PlayerOneAttack) {
          FighterAttack(firstFighter, secondFighter);
        }
        if (e.code === controls.PlayerTwoAttack) {
          FighterAttack(secondFighter, firstFighter);
        }
        if (controls.PlayerOneCriticalHitCombination.every(element => keys.has(element))) {
          FighterCriticalAttack(firstFighter);
        }
        if (controls.PlayerTwoCriticalHitCombination.every(element => keys.has(element))) {
          FighterCriticalAttack(secondFighter);
        }

    });

    function FighterBlocking(isFighter) {
      isFighter.statusBlocking = true;
    }

    function FighterAttack(AttackPlayer, DefensePlayer) {
        
      }
        
    }

    function FighterCriticalAttack(isFighter) {

    }    

    document.addEventListener('keyup', (e) => {
      if (e.code === controls.PlayerOneBlock) {
          PlayerOneBlocking = false;
      }
      if (e.code === controls.PlayerTwoBlock) {
          PlayerTwoBlocking = false;
        }
    });
    
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;  
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomFloatFromRange(1, 2);
  const attack = fighter.attack;
  const power = attack * criticalHitChance;
  return power;  
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomFloatFromRange(1, 2);
  const defense = fighter.defense;
  const power = defense * dodgeChance;
  return power;
}
