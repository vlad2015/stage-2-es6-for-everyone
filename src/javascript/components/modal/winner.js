import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
  const image = createFighterImage(fighter);
  const winner = {
    title: `Winner ${fighter.name}!`,
    bodyElement: image
  }
  
  showModal(winner);
}
