import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterDetailsContainer = createElement({
      tagName: 'div',
      className: 'fighter-container__details'
    });    
    const fighterDetails = createElement({
      tagName: 'div',
      className: 'fighter-details'
    });
    const fighterName = createElement({ tagName: 'h4', className: 'fighter-item__detail_title' });
    const fighterItemAttack = createElement({ tagName: 'div', className: 'fighter-item__detail' });
    fighterItemAttack.innerHTML = `<p class='fighter-item__detail__align_right'>Attack:  <span>${fighter.attack}</span> </p>`;
    fighterDetails.append(fighterItemAttack);
    const fighterItemDefense = createElement({ tagName: 'div', className: 'fighter-item__detail' });
    fighterItemDefense.innerHTML = `<p class='fighter-item__detail__align_right'>Defense: <span>${fighter.defense}</span> </p>`;
    fighterDetails.append(fighterItemDefense);
    const fighterItemHealth = createElement({ tagName: 'div', className: 'fighter-item__detail' });
    fighterItemHealth.innerHTML = `<p class='fighter-item__detail__align_right'>Health: <span>${fighter.health}</span> </p>`;
    fighterDetails.append(fighterItemHealth);
    fighterName.innerText = fighter.name;
    fighterDetailsContainer.append(fighterName, fighterDetails);
    fighterElement.append(fighterImg, fighterDetailsContainer);
  }
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
